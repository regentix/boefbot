<?php

namespace App\Controller;

use App\Entity\Attachment;
use App\Entity\Field;
use Exception;
use GuzzleHttp\Exception\GuzzleException;

use App\Entity\Body;
use App\Entity\Section;

use App\Service\DebugService;
use App\Service\PlacesService;
use App\Service\RequestService;
use App\Service\SlackService;

use App\Repository\TeamRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TestController
 * @package App\Controller
 */
class TestController extends AbstractController
{
    /**
     * @Route("/test", name="test", methods={"POST"})
     * @param Request $request
     * @param RequestService $requestService
     * @param TeamRepository $teamRepository
     * @param SlackService $slackService
     * @param DebugService $debug
     * @param PlacesService $places
     * @return Response
     */
    public function test
    (
        Request $request,
        RequestService $requestService,
        TeamRepository $teamRepository,
        SlackService $slackService,
        DebugService $debug,
        PlacesService $places
    ): Response
    {
        return new Response('Test controller is empty.');
    }
}
