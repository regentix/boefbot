<?php

namespace App\Controller;

use App\Entity\Attachment;
use App\Entity\Body;
use App\Entity\Field;
use App\Repository\TeamRepository;
use App\Service\PlacesService;
use App\Service\SlackService;
use App\Service\DebugService;
use App\Service\RequestService;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PollController
 * @package App\Controller
 */
class PollController extends AbstractController
{
    /**
     * @Route("/poll/create", name="poll_create", methods={"POST"})
     * @param Request $request
     * @param RequestService $requestService
     * @param TeamRepository $teamRepository
     * @param SlackService $slack
     * @param DebugService $debug
     * @param PlacesService $places
     * @return Response
     * @throws GuzzleException
     */
    public function index
    (
        Request $request,
        RequestService $requestService,
        TeamRepository $teamRepository,
        SlackService $slack,
        DebugService $debug,
        PlacesService $places
    )
    {
        $body = new Body();

        try {
            $team = $teamRepository->findOneBy(['teamId' => $requestService->getTeamId($request)]);

            $results = $places
                ->setSearchType('nearbysearch')
                ->setKeyWord($requestService->getText($request))
                ->setLatitude($team->getLocation()->getLatitude())
                ->setLongitude($team->getLocation()->getLongitude())
                ->setRadius(1000)
                ->search();

            usort($results['results'], function ($a, $b) {
                return $a['rating'] < $b['rating'];
            });

            $attachment = new Attachment();
            $attachment
                ->setPreText("*BoefPoll*\n" . count($results['results']) . " results for: " . $requestService->getText($request))
                ->setColor('#7cabe1');

            foreach ($results['results'] as $result) {
                $field = new Field();
                $field
                    ->setValue($result['vicinity'] . "\nrating: " . $result['rating'])
                    ->setTitle($result['name']);

                $attachment->addField($field);
            }

            $body->setAttachment($attachment);

            $slack::send($body->getObject(), $requestService->getWebHookUrl($request));
//            $debug::formatVarDump($results['results'][0]);
        } catch (Exception $exception) {
            $slack::send($debug::formatException($exception), $requestService->getWebHookUrl($request));
//            $debug::formatVarDump($body->getObject());
        }

        return new Response();
    }
}
