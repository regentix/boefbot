<?php

namespace App\Controller;

use App\Entity\Tag;
use App\Repository\TagRepository;
use App\Service\DebugService;
use App\Service\GeocodeService;
use App\Service\SlackService;
use Exception;
use App\Entity\Attachment;
use App\Entity\Body;
use App\Entity\Location;
use App\Entity\Team;
use App\Repository\TeamRepository;
use App\Service\RequestService;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeamController extends AbstractController
{
    private $requestService;
    private $teamRepository;
    private $tagRepository;
    private $entityManager;
    private $slackService;
    private $geocoder;
    private $debug;

    public function __construct(
        RequestService $requestService,
        TeamRepository $teamRepository,
        TagRepository $tagRepository,
        EntityManagerInterface $entityManager,
        SlackService $slackService,
        DebugService $debug,
        GeocodeService $geocoder
    )
    {
        $this->requestService = $requestService;
        $this->teamRepository = $teamRepository;
        $this->tagRepository = $tagRepository;
        $this->entityManager = $entityManager;
        $this->slackService = $slackService;
        $this->geocoder = $geocoder;
        $this->debug = $debug;
    }

    /**
     * Create the team address
     *
     * @Route("/team/address/create", name="team_address_create", methods={"POST"})
     * @param Request $request
     * @return Response
     * @throws GuzzleException
     */
    public function createAddress(Request $request): Response
    {
        try {
            // Get team
            $team = $this->getTeam($request);

            // Set location
            if (empty($team->getLocation())) {
                $location = new Location();
                $team->setLocation($location);
            }

            // Set address
            $team
                ->getLocation()
                ->setAddress($this->requestService->getText($request));

            // Geocode address
            $locationObject = $this->geocoder
                ->setAddress($team->getLocation()->getAddress())
                ->geocode();

            if ($locationObject['status'] === 'OK') {
                $team
                    ->getLocation()
                    ->setLatitude($locationObject['results'][0]['geometry']['location']['lat'])
                    ->setLongitude($locationObject['results'][0]['geometry']['location']['lng']);
            }

            $this->entityManager->flush();

            // Create formatted response
            $body = $this->debug->formatDefault('Team address successfully set.');

            // Send response body
            $this->slackService->send($body, $this->requestService->getWebHookUrl($request));
        } catch (Exception $exception) {
            $this->slackService->send($this->debug->formatException($exception), $this->requestService->getWebHookUrl($request));
        }

        return new Response();
    }

    /**
     * Add a single or multiple tags
     *
     * @Route("/team/tags/create", name="team_tags_create", methods={"POST"})
     * @param Request $request
     * @return Response
     * @throws GuzzleException
     */
    public function createTags(Request $request): Response
    {
        try {
            // Get team
            $team = $this->getTeam($request);

            // Get tag(s)
            $tags = $this->requestService->getText($request);

            $tags = explode(',', $tags);

            foreach ($tags as $text) {
                $text = $this->trimAndLower($text);
                $tag = $this->tagRepository->findOneBy([
                    'text' => $text,
                    'team' => $team
                ]);

                // Continue if the tag already exists
                if (!empty($tag)) continue;

                $tag = new Tag();
                $tag->setText($text);
                $team->addTag($tag);
            }

            $this->entityManager->flush();

            // Create formatted response
            $body = $this->debug->formatDefault('Tag(s) successfully added.');

            // Send response body
            $this->slackService->send($body, $this->requestService->getWebHookUrl($request));
        } catch (Exception $exception) {
            $this->slackService->send($this->debug->formatException($exception), $this->requestService->getWebHookUrl($request));
        }

        return new Response();
    }

    /**
     * Remove a single or multiple tags
     *
     * @Route("/team/tags/remove", name="team_tags_remove", methods={"POST"})
     * @param Request $request
     * @return Response
     * @throws GuzzleException
     */
    public function removeTags(Request $request): Response
    {
        try {
            $team = $this->getTeam($request);
            $tags = $this->requestService->getText($request);

            // Explode request text and trim + lower case values
            $tagsArr = array_map([$this, 'trimAndLower'], explode(',', $tags));

            foreach ($team->getTags() as $teamTag) {
                if (in_array($this->trimAndLower($teamTag->getText()), $tagsArr)) {
                    $this->entityManager->remove($teamTag);
                }
            }

            $this->entityManager->flush();

            // Create formatted response
            if (!empty($tags)) {
                $body = $this->debug->formatDefault('Tag(s) successfully removed.');
            } else {
                $body = $this->debug->formatDefault('Your team doesn\'t have any tags yet. Add one or more by typing /add-tag your, tags, here.');
            }

            // Send response body
            $this->slackService->send($body, $this->requestService->getWebHookUrl($request));

        } catch (Exception $exception) {
            $this->slackService->send($this->debug->formatException($exception), $this->requestService->getWebHookUrl($request));
        }

        return new Response();
    }

    /**
     * Get the team tags.
     *
     * @Route("/team/tags", name="team_tags_get", methods={"POST"})
     * @param Request $request
     * @return Response
     * @throws GuzzleException
     */
    public function getTags(Request $request): Response
    {
        try {
            // Get team
            $team = $this->getTeam($request);

            // Flush in case team is new
            $this->entityManager->flush();

            $tags = $team->getTags();
            $count = count($tags);

            $returnTags = '';

            for ($i = 0; $i < $count; $i++) {
                if ($i > 0) $returnTags .= ', ';
                $returnTags .= ucwords($tags[$i]->getText());
            }

            // Create formatted response
            if (!empty($tags)) {
                $body = $this->debug->formatDefault('These are the current search tags:', $returnTags, "#ff000");
            } else {
                $body = $this->debug->formatDefault('Your team doesn\'t have any tags yet. Add one or more by typing /add-tag your, tags, here.');
            }

            // Send response body
            $this->slackService->send($body, $this->requestService->getWebHookUrl($request));
        } catch (Exception $exception) {
            $this->slackService->send($this->debug->formatException($exception), $this->requestService->getWebHookUrl($request));
        }

        return new Response();
    }

    /**
     * Get team entity
     *
     * @param $request
     * @return Team
     */
    public function getTeam($request): Team
    {
        $team = $this->teamRepository->findOneBy([
            'teamId' => $this->requestService->getTeamId($request)
        ]);

        // Create team
        if (empty($team)) {
            $team = new Team();
            $team
                ->setTeamId($this->requestService->getTeamId($request))
                ->setDomain($this->requestService->getDomain($request));

            $this->entityManager->persist($team);
        }

        return $team;
    }

    /**
     * @param $value
     * @return string
     */
    public function trimAndLower($value) {
        return strtolower(trim($value));
    }
}
