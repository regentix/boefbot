<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Field
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $value;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $short = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Attachment", inversedBy="fields")
     * @ORM\JoinColumn(nullable=false)
     */
    private $attachment;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Section", inversedBy="fields")
     */
    private $section;

    /**
     * @return array
     */
    public function getObject(): ?array {
        return [
            'title' => !empty($this->getTitle()) ? $this->getTitle() : null,
            'value' => !empty($this->getValue()) ? $this->getValue() : null,
            'short' => !!$this->getValue(),
        ];
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return $this
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     * @return $this
     */
    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getShort(): ?bool
    {
        return $this->short;
    }

    /**
     * @param bool|null $short
     * @return $this
     */
    public function setShort(?bool $short): self
    {
        $this->short = $short;

        return $this;
    }

    /**
     * @return Attachment|null
     */
    public function getAttachment(): ?Attachment
    {
        return $this->attachment;
    }

    /**
     * @param Attachment|null $attachment
     * @return $this
     */
    public function setAttachment(?Attachment $attachment): self
    {
        $this->attachment = $attachment;

        return $this;
    }

    /**
     * @return Section|null
     */
    public function getSection(): ?Section
    {
        return $this->section;
    }

    /**
     * @param Section|null $section
     * @return $this
     */
    public function setSection(?Section $section): self
    {
        $this->section = $section;

        return $this;
    }
}
