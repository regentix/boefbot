<?php

namespace App\Entity;

use App\Interfaces\ResponseObject;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity()
 */
class Section implements ResponseObject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $textType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $blockId;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Field", mappedBy="section")
     */
    private $fields;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Body", inversedBy="sections")
     */
    private $body;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Attachment", inversedBy="sections")
     */
    private $attachment;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Accessory", cascade={"persist", "remove"})
     */
    private $accessory;

    /**
     * Section constructor.
     */
    public function __construct()
    {
        $this->fields = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     * @return Section
     */
    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTextType(): ?string
    {
        return $this->textType;
    }

    /**
     * @param string|null $textType
     * @return Section
     */
    public function setTextType(?string $textType): self
    {
        $this->textType = $textType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return Section
     */
    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBlockId(): ?string
    {
        return $this->blockId;
    }

    /**
     * @param string|null $blockId
     * @return Section
     */
    public function setBlockId(?string $blockId): self
    {
        $this->blockId = $blockId;

        return $this;
    }

    /**
     * @return Collection|Field[]
     */
    public function getFields(): Collection
    {
        return $this->fields;
    }

    /**
     * @param Field $field
     * @return Section
     */
    public function addField(Field $field): self
    {
        if (!$this->fields->contains($field)) {
            $this->fields[] = $field;
            $field->setSection($this);
        }

        return $this;
    }

    /**
     * @param Field $field
     * @return Section
     */
    public function removeField(Field $field): self
    {
        if ($this->fields->contains($field)) {
            $this->fields->removeElement($field);
            // set the owning side to null (unless already changed)
            if ($field->getSection() === $this) {
                $field->setSection(null);
            }
        }

        return $this;
    }

    /**
     * @return Body|null
     */
    public function getBody(): ?Body
    {
        return $this->body;
    }

    /**
     * @param Body|null $body
     * @return Section
     */
    public function setBody(?Body $body): self
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return Attachment|null
     */
    public function getAttachment(): ?Attachment
    {
        return $this->attachment;
    }

    /**
     * @param Attachment|null $attachment
     * @return Section
     */
    public function setAttachment(?Attachment $attachment): self
    {
        $this->attachment = $attachment;

        return $this;
    }

    /**
     * @return Accessory|null
     */
    public function getAccessory(): ?Accessory
    {
        return $this->accessory;
    }

    /**
     * @param Accessory|null $accessory
     * @return Section
     */
    public function setAccessory(?Accessory $accessory): self
    {
        $this->accessory = $accessory;

        return $this;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getObject(): array
    {
        // Set all params in object
        $object = $this->setObject();

        // Unset empty values and return object
        return $this->filterObject($object);
    }

    /**
     * @return array
     * @throws Exception
     */
    public function setObject(): array
    {
        return [
            'type' =>       empty($this->getType())         ? null : $this->getType(),
            'block_id' =>   empty($this->getBlockId())      ? null : $this->getBlockId(),
            'text' => [
                'type' =>   empty($this->getTextType())     ? null : $this->getTextType(),
                'text' =>   empty($this->getText())         ? null : $this->getText()
            ],
            'accessory' =>  empty($this->getAccessory())    ? null : $this->getAccessory()->getObject()
        ];
    }

    /**
     * @param array $object
     * @return array
     */
    public function filterObject(Array $object): array
    {
        if (empty($object['block_id']))       unset($object['block_id']);
        if (empty($object['accessory']))      unset($object['accessory']);
        if (empty($object['text']['type']))   unset($object['text']['type']);
        if (empty($object['text']['text']))   unset($object['text']['text']);
        if (empty($object['text']))           unset($object['text']);

        return $object;
    }
}
