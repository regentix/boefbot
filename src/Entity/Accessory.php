<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity()
 */
class Accessory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $placeholderType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $placeholderText;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $initialDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $actionId;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Option", mappedBy="accessory")
     */
    private $options;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Option", cascade={"persist", "remove"})
     */
    private $initialOption;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $imageUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $altText;

    /**
     * Accessory constructor.
     */
    public function __construct()
    {
        $this->options = new ArrayCollection();
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getObject() {
        // Populate response object
        $returnObject = [
            'action_id' =>      empty($this->getActionId()) ?           null : $this->getActionId(),
            'type' =>           empty($this->getType()) ?               null : $this->getType(),
            'placeholder' => [
                'type' =>       empty($this->getPlaceholderType()) ?    null : $this->getPlaceholderType(),
                'text' =>       empty($this->getPlaceholderText()) ?    null : $this->getPlaceholderText()
            ],
            'options' =>        empty($this->getOptionsObject()) ?      null : $this->getOptionsObject(),
            'image_url' =>      empty($this->getImageUrl()) ?           null : $this->getImageUrl(),
            'alt_text' =>       empty($this->getAltText()) ?            null : $this->getAltText()
        ];

        // Filter out empty arrays
        if (empty($returnObject['action_id']))  unset($returnObject['action_id']);


        return $returnObject;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return Accessory
     */
    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlaceholderType(): ?string
    {
        return $this->placeholderType;
    }

    /**
     * @param string|null $placeholderType
     * @return Accessory
     */
    public function setPlaceholderType(?string $placeholderType): self
    {
        $this->placeholderType = $placeholderType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlaceholderText(): ?string
    {
        return $this->placeholderText;
    }

    /**
     * @param string|null $placeholderText
     * @return Accessory
     */
    public function setPlaceholderText(?string $placeholderText): self
    {
        $this->placeholderText = $placeholderText;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getInitialDate(): ?DateTimeInterface
    {
        return $this->initialDate;
    }

    /**
     * @param DateTimeInterface|null $initialDate
     * @return Accessory
     */
    public function setInitialDate(?DateTimeInterface $initialDate): self
    {
        $this->initialDate = $initialDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getActionId(): ?string
    {
        return $this->actionId;
    }

    /**
     * @param string|null $actionId
     * @return Accessory
     */
    public function setActionId(?string $actionId): self
    {
        $this->actionId = $actionId;

        return $this;
    }

    /**
     * @return Collection|Option[]
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    /**
     * @param Option $option
     * @return Accessory
     */
    public function addOption(Option $option): self
    {
        if (!$this->options->contains($option)) {
            $this->options[] = $option;
            $option->setAccessory($this);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getOptionsObject() {
        $returnObject = [];
        foreach ($this->getOptions() as $option) {
            $returnObject[] = $option->getObject();
        }

        return $returnObject;
    }

    /**
     * @param Option $option
     * @return Accessory
     */
    public function removeOption(Option $option): self
    {
        if ($this->options->contains($option)) {
            $this->options->removeElement($option);
            // set the owning side to null (unless already changed)
            if ($option->getAccessory() === $this) {
                $option->setAccessory(null);
            }
        }

        return $this;
    }

    /**
     * @return Option|null
     */
    public function getInitialOption(): ?Option
    {
        return $this->initialOption;
    }

    /**
     * @param Option|null $initialOption
     * @return Accessory
     */
    public function setInitialOption(?Option $initialOption): self
    {
        $this->initialOption = $initialOption;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @param string|null $imageUrl
     * @return $this
     */
    public function setImageUrl(?string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAltText(): ?string
    {
        return $this->altText;
    }

    /**
     * @param string|null $altText
     * @return $this
     */
    public function setAltText(?string $altText): self
    {
        $this->altText = $altText;

        return $this;
    }
}
