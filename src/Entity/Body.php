<?php

namespace App\Entity;

use App\Interfaces\ResponseObject;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * @ORM\Entity()
 */
class Body implements ResponseObject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Attachment")
     */
    private $attachment;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Section", mappedBy="body")
     */
    private $sections;

    /**
     * Slack default is 'ephemeral', this makes it so only the caller of the slash command can see the response.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $responseType = "in_channel";

    /**
     * Body constructor.
     */
    public function __construct()
    {
        $this->sections = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Attachment|null
     */
    public function getAttachment(): ?Attachment
    {
        return $this->attachment;
    }

    /**
     * @param Attachment|null $attachment
     * @return $this
     */
    public function setAttachment(?Attachment $attachment): self
    {
        $this->attachment = $attachment;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getSections(): Collection
    {
        return $this->sections;
    }

    /**
     * @param Section $section
     * @return $this
     */
    public function addSection(Section $section): self
    {
        if (!$this->sections->contains($section)) {
            $this->sections[] = $section;
            $section->setBody($this);
        }

        return $this;
    }

    /**
     * @return array|null
     */
    public function getSectionsObject(): ?array
    {
        $returnObject = [];
        foreach ($this->getSections() as $section) {
            $returnObject[] = $section->getObject();
        }

        return $returnObject;
    }

    /**
     * @param Section $section
     * @return $this
     */
    public function removeSection(Section $section): self
    {
        if ($this->sections->contains($section)) {
            $this->sections->removeElement($section);
            // set the owning side to null (unless already changed)
            if ($section->getBody() === $this) {
                $section->setBody(null);
            }
        }

        return $this;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function setObject(): array
    {
        $object = [
            'response_type' => $this->getResponseType(),
            'blocks' => empty($this->getSectionsObject()) ? null : $this->getSectionsObject(),
            'attachments' => [empty($this->getAttachment()) ? null : $this->getAttachment()->getObject(),
                'blocks' => empty($this->getAttachment()) ? null : $this->getAttachment()->getSectionsObject(),
            ]
        ];

        if (empty($object['blocks']) &&
            empty($object['attachments'][0]) &&
            empty($object['attachments']['blocks'])
        ) $object = [
            'attachments' => [
                [
                    'title' => 'Empty body',
                    'text' => 'Please provide a valid response body.',
                    'color' => '#ff0000'
                ]
            ]
        ];

        return $object;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getObject(): array
    {
        $object = $this->setObject();
        $object = $this->filterObject($object);

        return $object;
    }

    /**
     * @param array $object
     * @return array
     */
    public function filterObject(array $object): array
    {
        if (empty($object['attachments']['blocks'])) unset($object['attachments']['blocks']);
        if (empty($object['attachments'][0])) unset($object['attachments']);
        if (empty($object['blocks'])) unset($object['blocks']);

        return $object;
    }

    /**
     * @return string|null
     */
    public function getResponseType(): ?string
    {
        return $this->responseType;
    }

    /**
     * @param string|null $responseType
     * @return $this
     */
    public function setResponseType(?string $responseType): self
    {
        $this->responseType = $responseType;

        return $this;
    }
}
