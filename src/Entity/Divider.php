<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

class Divider
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type = 'divider';

    /**
     * @return array|null
     */
    public function getObject(): ?array
    {
        return [
            'type' => $this->type
        ];
    }
}
