<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * @ORM\Entity()
 */
class Attachment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * A plain text summary of the attachment used in clients that don't show formatted text (eg. IRC, mobile notifications).
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fallback;

    /**
     * Changes the color of the border on the left side of this attachment from the default gray. Can either be one of good (green), warning (yellow), danger (red), or any hex color code (eg. #439FE0)
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * Text that appears above the message attachment block. It can be formatted as plain text, or with mrkdwn by including it in the mrkdwn_in field.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $preText;

    /**
     * Small text used to display the author's name.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $authorName;

    /**
     * A valid URL that will hyperlink the author_name text. Will only work if author_name is present.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $authorLink;

    /**
     * A valid URL that displays a small 16px by 16px image to the left of the author_name text. Will only work if author_name is present.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $authorIcon;

    /**
     * Large title text near the top of the attachment.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * A valid URL that turns the title text into a hyperlink.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titleLink;

    /**
     * The main body text of the attachment. It can be formatted as plain text, or with mrkdwn by including it in the mrkdwn_in field. The content will automatically collapse if it contains 700+ characters or 5+ linebreaks, and will display a "Show more..." link to expand the content.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $text;

    /**
     * A valid URL to an image file that will be displayed at the bottom of the attachment. We support GIF, JPEG, PNG, and BMP formats.
     * Large images will be resized to a maximum width of 360px or a maximum height of 500px, while still maintaining the original aspect ratio. Cannot be used with thumb_url.
     *
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $imageUrl;

    /**
     * A valid URL to an image file that will be displayed as a thumbnail on the right side of a message attachment. We currently support the following formats: GIF, JPEG, PNG, and BMP.
     * The thumbnail's longest dimension will be scaled down to 75px while maintaining the aspect ratio of the image. The file size of the image must also be less than 500 KB.
     * For best results, please use images that are already 75px by 75px.
     *
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $thumbUrl;

    /**
     * Some brief text to help contextualize and identify an attachment. Limited to 300 characters, and may be truncated further when displayed to users in environments with limited screen real estate.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $footer;

    /**
     * A valid URL to an image file that will be displayed beside the footer text. Will only work if author_name is present. We'll render what you provide at 16px by 16px. It's best to use an image that is similarly sized.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $footerIcon;

    /**
     * An integer Unix timestamp that is used to related your attachment to a specific time. The attachment will display the additional timestamp value as part of the attachment's footer.
     * Your message's timestamp will be displayed in varying ways, depending on how far in the past or future it is, relative to the present. Form factors, like mobile versus desktop may also transform its rendered appearance.
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $timeStamp;

    /**
     * An array of field objects that get displayed in a table-like way. For best results, include no more than 2-3 field objects.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Field", mappedBy="attachment")
     */
    private $fields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Section", mappedBy="attachment")
     */
    private $sections;

    /**
     * Attachment constructor.
     */
    public function __construct()
    {
        $this->fields = new ArrayCollection();
        $this->sections = new ArrayCollection();
    }

    /**
     * @return array
     */
    public function getObject(): ?array
    {
        return [
            'color' => $this->getColor(),
            'pretext' => $this->getPreText(),
            'author_name' => $this->getAuthorName(),
            'author_link' => $this->getAuthorLink(),
            'author_icon' => $this->getAuthorIcon(),
            'title' => $this->getTitle(),
            'title_link' => $this->getTitleLink(),
            'text' => $this->getText(),
            'fields' => $this->getFieldsObject(),
            'thumb_url' => $this->getThumbUrl(),
            'footer' => $this->getFooter(),
            'footer_icon' => $this->getFooterIcon(),
            'ts' => $this->getTimeStamp()
        ];
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getFallback(): ?string
    {
        return $this->fallback;
    }

    /**
     * @param string|null $fallback
     * @return Attachment
     */
    public function setFallback(?string $fallback): self
    {
        $this->fallback = $fallback;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return Attachment
     */
    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPreText(): ?string
    {
        return $this->preText;
    }

    /**
     * @param string|null $preText
     * @return Attachment
     */
    public function setPreText(?string $preText): self
    {
        $this->preText = $preText;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthorName(): ?string
    {
        return $this->authorName;
    }

    /**
     * @param string|null $authorName
     * @return Attachment
     */
    public function setAuthorName(?string $authorName): self
    {
        $this->authorName = $authorName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthorLink(): ?string
    {
        return $this->authorLink;
    }

    /**
     * @param string|null $authorLink
     * @return Attachment
     */
    public function setAuthorLink(?string $authorLink): self
    {
        $this->authorLink = $authorLink;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthorIcon(): ?string
    {
        return $this->authorIcon;
    }

    /**
     * @param string|null $authorIcon
     * @return Attachment
     */
    public function setAuthorIcon(?string $authorIcon): self
    {
        $this->authorIcon = $authorIcon;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return Attachment
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitleLink(): ?string
    {
        return $this->titleLink;
    }

    /**
     * @param string|null $titleLink
     * @return Attachment
     */
    public function setTitleLink(?string $titleLink): self
    {
        $this->titleLink = $titleLink;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     * @return Attachment
     */
    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @param string|null $imageUrl
     * @return Attachment
     */
    public function setImageUrl(?string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getThumbUrl(): ?string
    {
        return $this->thumbUrl;
    }

    /**
     * @param string|null $thumbUrl
     * @return Attachment
     */
    public function setThumbUrl(?string $thumbUrl): self
    {
        $this->thumbUrl = $thumbUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFooter(): ?string
    {
        return $this->footer;
    }

    /**
     * @param string|null $footer
     * @return Attachment
     */
    public function setFooter(?string $footer): self
    {
        $this->footer = $footer;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFooterIcon(): ?string
    {
        return $this->footerIcon;
    }

    /**
     * @param string|null $footerIcon
     * @return Attachment
     */
    public function setFooterIcon(?string $footerIcon): self
    {
        $this->footerIcon = $footerIcon;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getTimeStamp(): ?int
    {
        return $this->timeStamp;
    }

    /**
     * @param int|null $timeStamp
     * @return Attachment
     */
    public function setTimeStamp(?int $timeStamp): self
    {
        $this->timeStamp = $timeStamp;

        return $this;
    }

    /**
     * @return Collection|Field[]
     */
    public function getFields(): Collection
    {
        return $this->fields;
    }

    /**
     * @param Field $field
     * @return Attachment
     */
    public function addField(Field $field): self
    {
        if (!$this->fields->contains($field)) {
            $this->fields[] = $field;
            $field->setAttachment($this);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getFieldsObject(): ?array
    {
        $returnObject = [];
        foreach ($this->getFields() as $field) {
            $returnObject[] = $field->getObject();
        }

        return $returnObject;
    }

    /**
     * @param Field $field
     * @return Attachment
     */
    public function removeField(Field $field): self
    {
        if ($this->fields->contains($field)) {
            $this->fields->removeElement($field);
            // set the owning side to null (unless already changed)
            if ($field->getAttachment() === $this) {
                $field->setAttachment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Section[]
     */
    public function getSections(): Collection
    {
        return $this->sections;
    }

    /**
     * @param Section $section
     * @return Attachment
     */
    public function addSection(Section $section): self
    {
        if (!$this->sections->contains($section)) {
            $this->sections[] = $section;
            $section->setAttachment($this);
        }

        return $this;
    }

    /**
     * @param Section $section
     * @return $this
     */
    public function removeSection(Section $section): self
    {
        if ($this->sections->contains($section)) {
            $this->sections->removeElement($section);
            // set the owning side to null (unless already changed)
            if ($section->getAttachment() === $this) {
                $section->setAttachment(null);
            }
        }

        return $this;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getSectionsObject(): ?array
    {
        $returnObject = [];
        foreach ($this->getSections() as $section) {
            $returnObject[] = $section->getObject();
        }

        return $returnObject;
    }
}
