<?php


namespace App\Interfaces;


interface ResponseObject
{
    public function setObject(): array;
    public function getObject(): array;
    public function filterObject(Array $object): array;
}