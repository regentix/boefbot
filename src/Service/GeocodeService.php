<?php

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class GeocodeService
{
    private $baseUrl = 'https://maps.googleapis.com/maps/api/geocode/json';
    private $address;

    /**
     * @param mixed $address
     * @return GeocodeService
     */
    public function setAddress($address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * @return string
     */
    public function getApiUrl()
    {
        return $this->getBaseUrl() . '?address=' . urlencode($this->getAddress()) . '&key=' . $_ENV['GOOGLE_API_KEY'];
    }

    /**
     * @return int
     * @throws GuzzleException
     */
    public function geocode()
    {
        $client = new Client(['headers' => ['Content-Type' => 'application/json']]);
        $response = $client->request('GET', $this->getApiUrl());

        return json_decode($response->getBody(), true);
    }
}