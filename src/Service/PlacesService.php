<?php

namespace App\Service;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class PlacesService
{
    private $baseUrl = 'https://maps.googleapis.com/maps/api/place/';
    private $outputType = 'json';
    private $searchType;
    private $radius = 2000;
    private $keyWord;
    private $longitude;
    private $latitude;
    private $pageToken = null;

    /**
     * @param mixed $latitude
     * @return PlacesService
     */
    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $longitude
     * @return PlacesService
     */
    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * @return mixed
     */
    public function getKeyWord()
    {
        return $this->keyWord;
    }

    /**
     * @param mixed $keyWord
     * @return PlacesService
     */
    public function setKeyWord($keyWord): self
    {
        $this->keyWord = $keyWord;

        return $this;
    }

    /**
     * @param mixed $radius
     * @return PlacesService
     */
    public function setRadius($radius): self
    {
        $this->radius = $radius;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * @return mixed
     */
    public function getSearchType()
    {
        return $this->searchType;
    }

    /**
     * @param mixed $searchType
     * @return PlacesService
     */
    public function setSearchType($searchType): self
    {
        $this->searchType = $searchType;

        return $this;
    }

    /**
     * @param string $outputType
     * @return PlacesService
     */
    public function setOutputType(string $outputType): self
    {
        $this->outputType = $outputType;

        return $this;
    }

    /**
     * @return string
     */
    public function getOutputType(): string
    {
        return $this->outputType;
    }

    /**
     * @param null $pageToken
     * @return PlacesService
     */
    public function setPageToken($pageToken): self
    {
        $this->pageToken = $pageToken;

        return $this;
    }

    /**
     * @return null
     */
    public function getPageToken()
    {
        return $this->pageToken;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getApiUrl()
    {
        $url = $this->getBaseUrl() . $this->getSearchType() . '/' . $this->getOutputType();

        if (!$this->getPageToken()) {
            // Init params
            $url .= '?type=restaurant';

            if (!empty($this->getKeyWord())) {
                $url .= '&keyword=' . urlencode($this->getKeyWord());
            }

            if (!empty($this->getLongitude()) && !empty($this->getLatitude())) {
                $url .= '&location=' . $this->getLatitude() . ',' . $this->getLongitude();
            } else {
                throw new Exception('It seems like your team doesn\'t have a geolocation yet. Please run /boefbot:address followed with your team address.');
            }


            $url .= '&radius=' . $this->getRadius();
            //$url .= '&opennow=true';
        } else {
            $url .= '?pagetoken=' . $this->getPageToken();
        }

        $url .= '&key=' . $_ENV['GOOGLE_API_KEY'];

        return $url;
    }

    /**
     * @return array
     * @throws GuzzleException|Exception
     */
    public function search()
    {
        $client = new Client(['headers' => ['Content-Type' => 'application/json']]);
        $response = $client->request('GET', $this->getApiUrl());

        return json_decode($response->getBody(), true);
    }
}