<?php

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class MessageService
 * @package App\Service
 */
class SlackService
{
    /**
     * @param $body
     * @param $url
     * @return void
     * @throws GuzzleException
     */
    public static function send($body, $url) : void
    {
        $client = new Client(['headers' => ['Content-Type' => 'application/json']]);
        $client->request('POST', $url, ['body' => json_encode($body)]);

        return;
    }
}