<?php

namespace App\Service;

use App\Entity\Attachment;
use App\Entity\Body;
use Exception;

/**
 * Class DebugService
 * @package App\Service
 */
class DebugService
{
    /**
     * @param Exception $exception
     * @return array
     */
    public static function formatException(Exception $exception): array
    {
        return [
            'attachments' => [
                [
                    'title' => $exception->getMessage(),
                    'text' => $exception->getFile() . ': ' . $exception->getLine(),
                    'color' => '#ff0000'
                ]
            ]
        ];
    }

    /**
     * @param string $title
     * @param string $text
     * @param string $color
     * @return array
     */
    public static function formatDefault(string $title = '', string $text = '', string $color = '#69ff91'): array
    {
        return [
            'attachments' => [
                [
                    'title' => $title,
                    'text' => $text,
                    'color' => $color
                ]
            ]
        ];
    }

    /**
     * @param $dump
     */
    public static function formatVarDump($dump): void
    {
        echo '```';
        var_dump($dump);
        echo '```';
    }
}