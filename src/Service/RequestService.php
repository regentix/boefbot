<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class RequestService
 * @package App\Service
 */
class RequestService
{
    /**
     * @param Request $request
     * @return array
     */
    public function getContent(Request $request) : array
    {
        return $request->request->all();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getParameters(Request $request) : array
    {
        $textData = explode(' ', $this->getText($request));

        $parameters = [
            'when' => (string) $textData[0],
            'distance' => (int) $textData[1],
            'rating' => (float) $textData[2],
        ];

        for ($i = 3; $i < count($textData); $i++) {
            $parameters[] = $textData[$i];
        }

        return $parameters;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getText(Request $request) : string
    {
        $requestData = $this->getContent($request);

        return $requestData['text'];
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getTeamId(Request $request) : string
    {
        $requestData = $this->getContent($request);

        return $requestData['team_id'];
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getDomain(Request $request) : string
    {
        $requestData = $this->getContent($request);

        return $requestData['team_domain'];
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getUser(Request $request) : string
    {
        $requestData = $this->getContent($request);

        return $requestData['user_name'];
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getWebHookUrl(Request $request) : string
    {
        $requestData = $this->getContent($request);

        return $requestData['response_url'];
    }
}