<?php

namespace App\Command;

use Doctrine\Common\Collections\Collection;
use Exception;
use App\Entity\Location;
use App\Entity\Place;
use App\Entity\Tag;
use App\Entity\Team;
use App\Repository\PlaceRepository;
use App\Repository\TeamRepository;
use App\Service\PlacesService;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PlacesUpdateCommand extends Command
{
    /**
     * @var Team
     */
    protected $team;
    protected $entityManager;
    protected $places;
    protected $teamRepository;
    protected $placeRepository;
    protected $count;

    protected static $defaultName = 'places:update';

    public function __construct
    (
        PlacesService $placesService,
        TeamRepository $teamRepository,
        PlaceRepository $placeRepository,
        EntityManagerInterface $entityManager,
        string $name = null
    )
    {
        $this->entityManager = $entityManager;
        $this->places = $placesService;
        $this->teamRepository = $teamRepository;
        $this->placeRepository = $placeRepository;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Fetch and update/create places from the Google Places API.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $this->team = $this->teamRepository->findOneBy([
                'domain' => 'regentixworkspace'
            ]);

            $tags = $this->team->getTags();
            $this->processTags($tags, $io);

        } catch (Exception $e) {
            $io->text(sprintf($e->getFile()) . ':' . $e->getLine());
        }

        $io->success('Places successfully updated.');
    }

    /**
     * @param $tags Collection
     * @param $io SymfonyStyle
     * @throws GuzzleException
     */
    public function processTags($tags, $io)
    {
        foreach ($tags as $tag) {
            $this->count = 0;

            // Get results
            $results = $this->places
                ->setSearchType('nearbysearch')
                ->setKeyWord(urlencode($tag->getText()))
                ->setLatitude($this->team->getLocation()->getLatitude())
                ->setLongitude($this->team->getLocation()->getLongitude())
                ->search();

            // Create or update places
            $this->handleResults($results, $tag, $io);

            $tag = $tag->getText();

            if ($this->count == 0) {
                $io->error("Finished processing: $tag with $this->count results");
            } else {
                $io->success("Finished processing: $tag with $this->count results");
            }
            $io->newLine(1);
        }
    }

    /**
     * @param $results array
     * @param $tag Tag
     * @param $io SymfonyStyle
     * @return void
     * @throws GuzzleException
     */
    public function handleResults($results, $tag, $io)
    {
        $this->count += count($results['results']);
        $io->text(count($results['results']) . " results for: " . $tag->getText());
        $io->newLine(1);

        // Create or update places
        $io->text("Setting places for: " . $tag->getText());
        $this->setPlaces($results, $tag);
        $io->text("Finished setting places for: " . $tag->getText());
        $io->newLine(1);

        if (!empty($results['next_page_token'])) {
            $io->text("Sleeping 2 seconds");
            // There is a short delay between when a next_page_token is issued,
            // and when it will become valid.
            sleep(2);
            $io->text("Waking up");
            $io->newLine(1);

            // Get next page results
            $io->warning("Getting next page results for: " . $tag->getText());
            $netPageResults = $this->places
                ->setPageToken($results['next_page_token'])
                ->search();

            $io->success("Finished getting places next places, repeating function");
            $io->newLine(1);
            $this->handleResults($netPageResults, $tag, $io);
        }

        // Reset url params
        $this->places
            ->setPageToken(null)
            ->setKeyWord(null);
    }

    /**
     * @param $results
     * @param $tag Tag
     */
    public function setPlaces($results, $tag)
    {
        foreach ($results['results'] as $result) {
            $place = $this->placeRepository->findOneBy([
                    'uuid' => $result['id']
                ]);

            if (empty($place)) {
                $place = new Place();
                $location = new Location();
                $place->setLocation($location);

                $this->entityManager->persist($place);
            }

            $place
                ->getLocation()
                ->setAddress($result['vicinity'])
                ->setLatitude($result['geometry']['location']['lat'])
                ->setLongitude($result['geometry']['location']['lng']);

            $place
                ->setUuid($result['id'])
                ->setRatings($result['user_ratings_total'])
                ->setName($result['name'])
                ->setRating($result['rating']);

            $tag->addPlace($place);
        }

        $this->entityManager->flush();
    }
}
